"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const i = require("immutable");
const chai_1 = require("chai");
const index_1 = require("../index");
describe("test index", () => {
    let pool = new index_1.Pool((p, e) => new index_1.EntityWrapper(p, e));
    it("add an entity", () => {
        let e = pool.entity();
        chai_1.expect(pool.entity(e.uid).components).is.deep.equal({});
    });
    let status = i.Map({ hp: 10, mp: 10 });
    let name = i.Map({ id: 0, str: "Alice" });
    it("create withcomponents", () => {
        let e = pool.entity({ name, status });
        let uid = e.uid;
        let e1 = pool.entity(uid).clone();
        e.updateWith("status", { hp: 9 });
        chai_1.expect(e.components["status"].get("hp")).is.eq(9);
        chai_1.expect(pool.entity(uid).getComponent("status").get("hp")).is.eq(9);
    });
    it("collector: collectorToArray, collectorDo", () => {
        let weapon = i.Map({ type: "sword", atk: 10 });
        pool.entity({ weapon }).clone();
        // collectorToArray
        let arr = pool.collectorToArray("weapon");
        chai_1.expect(arr.length).is.eq(2);
        pool.entity(arr[0]).clone();
        chai_1.expect(pool.collectorToArray("weapon").length).is.eq(3);
        // collectorDo
        let arr0 = pool.collectorToArray("weapon");
        pool.collectorDo("weapon", e => {
            chai_1.expect(_.includes(arr0, e.uid)).ok;
        });
    });
    it("tick", () => {
        chai_1.expect(pool.changes.add.length).is.greaterThan(0);
        pool.tick();
        chai_1.expect(pool.changes.add.length).is.eq(0);
    });
});
