import * as i from 'immutable';
import { IEntity, IComponent, IPool, IWrapper } from './interface';
export { IEntity, IComponent, IPool, IWrapper };
export declare type IUpdateItem = [string, string, IComponent | undefined];
export interface IChanges {
    add: IUpdateItem[];
    update: IUpdateItem[];
    delete: IUpdateItem[];
}
export declare class Pool<TWrapper extends IWrapper> implements IPool {
    map: Map<string, IEntity>;
    wrapper: (pool: IPool, entity: IEntity) => TWrapper;
    changes: IChanges;
    private getCollector;
    constructor(wrapper: (pool: IPool, entity: IEntity) => TWrapper);
    collectorDo(componentName: string, func: (e: IEntity) => void): void;
    collectorToArray(componentName: string): string[];
    entity(): TWrapper;
    entity(id: string): TWrapper;
    entity(components: {
        [index: string]: any;
    }): TWrapper;
    destroy(id: string): boolean;
    destroy(entity: IEntity): boolean;
    notify(action: 'delete', uid: string, componentName: string): any;
    notify(action: 'add' | 'update', uid: string, componentName: string, newComponent: IComponent): any;
    updateComponent(uid: string, componentName: string, oldComponent: IComponent | null, newComponent: IComponent | null): void;
    tick(): void;
}
export declare class EntityWrapper implements IWrapper {
    private pool;
    private entity;
    readonly uid: string;
    readonly components: {
        [index: string]: i.Map<string, any>;
    };
    constructor(pool: IPool, entity: IEntity);
    getComponent(componentName: string): i.Map<string, any>;
    clone(): IWrapper;
    cloneComponent(componentName: string, other: IEntity): this;
    add(componentName: string, component: IComponent, opts: any): this;
    update(componentName: string, func: (mutation: i.Map<string, any>) => void): this;
    updateWith(componentName: string, opts: any): this;
    delete(componentName: string): this;
    destroy(): void;
}
