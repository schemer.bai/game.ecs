"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
class Pool {
    constructor(wrapper) {
        this.changes = {
            add: [],
            update: [],
            delete: []
        };
        this.map = new Map();
        this.wrapper = wrapper;
        this.getCollector = _.memoize((componentName) => {
            let ret = new Set();
            this.map.forEach((entity, uid) => {
                if (entity.components[componentName])
                    ret.add(uid);
            });
            return ret;
        });
    }
    collectorDo(componentName, func) {
        this.getCollector(componentName).forEach(v => {
            let e = this.map.get(v);
            e && func(e);
        });
    }
    collectorToArray(componentName) {
        return Array.from(this.getCollector(componentName));
    }
    entity(param) {
        if (_.isString(param)) {
            let entity = this.map.get(param);
            if (entity) {
                return this.wrapper(this, entity);
            }
            else {
                throw 'entity not exist';
            }
        }
        else {
            let entity = {
                uid: _.uniqueId('Entity_'),
                components: {}
            };
            this.map.set(entity.uid, entity);
            if (param) {
                entity.components = Object.assign({}, param);
                _.each(entity.components, (component, componentName) => {
                    this.notify("add", entity.uid, componentName, component);
                });
            }
            return this.wrapper(this, entity);
        }
    }
    destroy(param) {
        if (_.isString(param)) {
            let entity = this.map.get(param);
            if (entity) {
                this.map.delete(param);
                _.each(entity.components, (v, componentName) => {
                    this.notify("delete", param, componentName);
                });
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return this.destroy(param.uid);
        }
    }
    notify(action, uid, componentName, newComponent) {
        this.changes[action].push([uid, componentName, newComponent]);
        if (this.getCollector.cache.has(componentName)) {
            if (action === 'add') {
                this.getCollector.cache.get(componentName).add(uid);
            }
            else if (action === 'delete') {
                this.getCollector.cache.get(componentName).delete(uid);
            }
        }
    }
    updateComponent(uid, componentName, oldComponent, newComponent) {
        let entity = this.map.get(uid);
        if (entity) {
            // add
            if (!oldComponent && newComponent) {
                entity.components[componentName] = newComponent;
                this.notify("add", uid, componentName, newComponent);
            }
            else if (!newComponent) {
                delete entity[componentName];
                this.notify("delete", uid, componentName);
            }
            else if (oldComponent && newComponent) {
                // skip update, if not changed
                if (oldComponent.equals(newComponent))
                    return;
                entity.components[componentName] = newComponent;
                this.notify("update", uid, componentName, newComponent);
            }
            else {
                throw 'update component error, expect oldComponent || newComponent';
            }
        }
        else {
            throw 'do not have this entity: ' + uid;
        }
    }
    tick() {
        this.changes.add = [];
        this.changes.update = [];
        this.changes.delete = [];
    }
}
exports.Pool = Pool;
class EntityWrapper {
    get uid() {
        return this.entity.uid; // readonly
    }
    get components() {
        return Object.assign({}, this.entity.components); // readonly
    }
    constructor(pool, entity) {
        this.pool = pool;
        this.entity = entity;
    }
    getComponent(componentName) {
        return this.entity.components[componentName];
    }
    clone() {
        return this.pool.entity(this.entity.components);
    }
    cloneComponent(componentName, other) {
        this.pool.updateComponent(this.entity.uid, componentName, null, other.components[componentName]);
        return this;
    }
    add(componentName, component, opts) {
        this.pool.updateComponent(this.entity.uid, componentName, null, component.merge(opts));
        return this;
    }
    update(componentName, func) {
        let comp = this.components[componentName];
        this.pool.updateComponent(this.entity.uid, componentName, comp, comp.withMutations(func));
        return this;
    }
    updateWith(componentName, opts) {
        let comp = this.components[componentName];
        this.pool.updateComponent(this.entity.uid, componentName, comp, comp.merge(opts));
        return this;
    }
    delete(componentName) {
        let comp = this.components[componentName];
        this.pool.updateComponent(this.entity.uid, componentName, comp, null);
        return this;
    }
    destroy() {
        this.pool.destroy(this.entity.uid);
    }
}
exports.EntityWrapper = EntityWrapper;
