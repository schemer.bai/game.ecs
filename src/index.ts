import * as _ from 'lodash'
import * as i from 'immutable'

import { IEntity, IComponent, IPool, IWrapper } from './interface'
export { IEntity, IComponent, IPool, IWrapper }

export type IUpdateItem = [string, string, IComponent | undefined]

export interface IChanges {
  add: IUpdateItem[]
  update: IUpdateItem[]
  delete: IUpdateItem[]
}

export class Pool<TWrapper extends IWrapper> implements IPool {

  map: Map<string, IEntity>

  wrapper: (pool: IPool, entity: IEntity) => TWrapper

  changes: IChanges = {
    add: [],
    update: [],
    delete: []
  }

  private getCollector: ((componentName: string) => Set<string>) & _.MemoizedFunction

  constructor(wrapper: (pool: IPool, entity: IEntity) => TWrapper) {
    this.map = new Map()
    this.wrapper = wrapper
    this.getCollector = _.memoize((componentName: string) => {
      let ret = new Set<string>()
      this.map.forEach((entity, uid) => {
        if (entity.components[componentName]) ret.add(uid)
      })
      return ret
    })
  }

  collectorDo(componentName: string, func: (e: IEntity) => void) {
    this.getCollector(componentName).forEach(v => {
      let e = this.map.get(v)
      e && func(e)
    })
  }

  collectorToArray(componentName: string) {
    return Array.from(this.getCollector(componentName))
  }

  entity(): TWrapper
  entity(id: string): TWrapper
  entity(components: {[index: string]: any}): TWrapper
  entity(param?: { [index: string]: any } | string): TWrapper {
    if (_.isString(param)) {
      let entity = this.map.get(param)
      if (entity) {
        return this.wrapper(this, entity)
      }
      else {
        throw 'entity not exist'
      }
    }
    else {
      let entity: IEntity = {
        uid: _.uniqueId('Entity_'),
        components: {}
      }
      this.map.set(entity.uid, entity)

      if (param) {
        entity.components = {...param}
        _.each(entity.components, (component, componentName) => {
          this.notify("add", entity.uid, componentName, component)
        })
      }

      return this.wrapper(this, entity)
    }
  }

  destroy(id: string): boolean
  destroy(entity: IEntity): boolean
  destroy(param: string | IEntity): boolean {
    if (_.isString(param)) {
      let entity = this.map.get(param)
      if(entity) {
        this.map.delete(param)
        _.each(entity.components, (v, componentName) => {
          this.notify("delete", param, componentName)
        })
        return true
      }
      else {
        return false
      }
    }
    else {
      return this.destroy(param.uid)
    }
  }

  notify(action: 'delete', uid: string, componentName: string)
  notify(action: 'add' | 'update', uid: string, componentName: string, newComponent: IComponent)
  notify(action: 'add' | 'update' | 'delete', uid: string, componentName: string, newComponent?: IComponent) {
    this.changes[action].push([uid, componentName, newComponent])
    if (this.getCollector.cache.has(componentName)) {
      if (action === 'add') {
        (this.getCollector.cache.get(componentName) as Set<string>).add(uid)
      }
      else if (action === 'delete') {
        (this.getCollector.cache.get(componentName) as Set<string>).delete(uid)
      }
    }
  }

  updateComponent(
    uid: string,
    componentName: string,
    oldComponent: IComponent | null,
    newComponent: IComponent | null
  ) {

    let entity = this.map.get(uid)
    if (entity) {
      // add
      if (!oldComponent && newComponent) {
        entity.components[componentName] = newComponent
        this.notify("add", uid, componentName, newComponent)
      }
      // delete
      else if (!newComponent) {
        delete entity[componentName]
        this.notify("delete", uid, componentName)
      }
      // update
      else if (oldComponent && newComponent) {
        // skip update, if not changed
        if (oldComponent.equals(newComponent)) return 
        entity.components[componentName] = newComponent
        this.notify("update", uid, componentName, newComponent)
      }
      else {
        throw 'update component error, expect oldComponent || newComponent'
      }
    }
    else {
      throw 'do not have this entity: ' + uid
    }
  }

  tick() {
    this.changes.add = []
    this.changes.update = []
    this.changes.delete = []
  }

}

export class EntityWrapper implements IWrapper {

  private pool: IPool
  private entity: IEntity

  get uid() {
    return this.entity.uid // readonly
  }

  get components() {
    return { ...this.entity.components } // readonly
  }

  constructor(pool: IPool, entity: IEntity) {
    this.pool = pool
    this.entity = entity
  }

  getComponent(componentName: string) {
    return this.entity.components[componentName]
  }

 clone() {
    return this.pool.entity(this.entity.components)
  }

  cloneComponent(componentName: string, other: IEntity) {
    this.pool.updateComponent(
      this.entity.uid, componentName, null, other.components[componentName]
    )
    return this
  }

  add(componentName: string, component: IComponent, opts: any) {
    this.pool.updateComponent(
      this.entity.uid, componentName, null, component.merge(opts)
    )
    return this
  }

  update(componentName: string, func: (mutation: i.Map<string, any>) => void) {
    let comp = this.components[componentName]
    this.pool.updateComponent(
      this.entity.uid, componentName, comp, comp.withMutations(func))
    return this
  }

  updateWith(componentName: string, opts: any) {
    let comp = this.components[componentName]
    this.pool.updateComponent(
      this.entity.uid, componentName, comp, comp.merge(opts)
    )
    return this
  }

  delete(componentName: string) {
    let comp = this.components[componentName]
    this.pool.updateComponent(this.entity.uid, componentName, comp, null)
    return this
  }

  destroy() {
    this.pool.destroy(this.entity.uid)
  }
}