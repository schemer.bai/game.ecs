import * as i from 'immutable'

export type IComponent = i.Map<string, any>

export interface IEntity {
  uid: string // unique id
  components: {[index: string]: IComponent}
}

export interface IPool {

  collectorDo(componentName: string, func: (e: IEntity) => void): void
  collectorToArray(componentName: string): string[]

  entity(): IWrapper
  entity(id: string): IWrapper
  entity(components: {[index: string]: any}): IWrapper

  destroy(id: string): boolean
  destroy(entity: IEntity): boolean

  updateComponent(
    uid: string,
    componentName: string,
    oldComponent: IComponent | null,
    newComponent: IComponent | null
  ): void

  /**
   * after a tick, the changes will be cleaned
   */
  tick(): void
}

export interface IWrapper {

  readonly uid: string
  readonly components: {[index: string]: IComponent}
  getComponent(componentName: string): IComponent

  cloneComponent(componentName: string, other: IEntity): IWrapper
  add(componentName: string, component: IComponent, opts: any): IWrapper
  update(componentName: string, func: () => void): IWrapper
  updateWith(componentName: string, opts: any): IWrapper
  delete(componentName: string): IWrapper

  clone(): IWrapper 
  destroy(): void
}