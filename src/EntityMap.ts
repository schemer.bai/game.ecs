import { RefMap } from './RefMap'

interface IComponent { }

interface IEntity {
  [index: string]: IComponent
}

class Pool {

  refMap = new RefMap<IEntity>()

  entity(template?: IEntity) {
    this.refMap.create(template || {}, (entity, ref) => {

    })
  }

  get(ref: string) {
    return this.refMap.get(ref)
  }

  update(ref: string, entity: IEntity) {
    this.refMap.update(ref, lastEntity => {
      return entity
    })
  }

  destroy(ref: string) {
    this.refMap.destroy(ref, entity => {

    })
  }

  *iterator() {
    return this.refMap.iterator()
  }
}