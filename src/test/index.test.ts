import * as _ from 'lodash'
import * as i from 'immutable'
import { expect } from 'chai'
import { Pool, EntityWrapper} from '../index'

describe("test index", () => {

  let pool = new Pool((p, e) => new EntityWrapper(p, e))

  it("add an entity", () => {

    let e = pool.entity()
    expect(pool.entity(e.uid).components).is.deep.equal({})
  })

  let status = i.Map({ hp: 10, mp: 10 })
  let name = i.Map({ id: 0, str: "Alice" })

  it("create withcomponents", () => {

    let e = pool.entity({ name, status })
    let uid = e.uid
    let e1 = pool.entity(uid).clone()

    e.updateWith("status", { hp: 9 })

    expect(e.components["status"].get("hp")).is.eq(9)
    expect(pool.entity(uid).getComponent("status").get("hp")).is.eq(9)
  })


  it("collector: collectorToArray, collectorDo", () => {

    let weapon = i.Map({ type: "sword", atk: 10 })
    pool.entity({ weapon }).clone()

    // collectorToArray
    let arr = pool.collectorToArray("weapon")
    expect(arr.length).is.eq(2)

    pool.entity(arr[0]).clone()
    expect(pool.collectorToArray("weapon").length).is.eq(3)

    // collectorDo
    let arr0 = pool.collectorToArray("weapon")
    pool.collectorDo("weapon", e => {
      expect(_.includes(arr0, e.uid)).ok
    })
  })

  it("tick", () => {
    
    expect(pool.changes.add.length).is.greaterThan(0)
    pool.tick()
    expect(pool.changes.add.length).is.eq(0)
  })

})