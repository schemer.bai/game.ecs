/** 
 * A Map of reference
 * just delete this map when memory leaked
*/

interface RefObject<T> {
  ref: string
  data: T
}

export class RefMap<TData> {

  private map: Map<string, RefObject<TData>> = new Map()
  private count = 0 // to generate id

  create(
    data: TData,
    cb: (data: TData, ref: string) => void
  ) {
    let ref = "ref_" + (this.count++)
    this.map.set(ref, { ref, data })
    cb(data, ref) // after add to the map
    return ref
  }

  update(
    ref: string,
    cb: (lastData: TData) => TData
  ){
    let robj = this.getRefObj(ref)
    robj.data = cb(robj.data)
  }

  destroy(
    ref: string,
    cb: (data: TData) => void
  ) {
    let robj = this.map.get(ref)
    if (robj) {
      this.map.delete(ref)
      cb(robj.data) // after delete from the map
    }
  }

  get(ref: string) {
    return this.getRefObj(ref).data
  }

  private getRefObj(ref: string) {
    let robj = this.map.get(ref)
    if (robj) return robj
    else throw 'do not find the uid of: ' + ref
  }

  *iterator(): IterableIterator<[string, TData]> {
    for (let e of this.map) {
      yield [e[0], e[1].data]
    }
  }

}